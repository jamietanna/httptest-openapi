# Go OpenAPI Contract Verification for use with `net/http`

A Go library for making it easier to validate that your [OpenAPI contracts](https://www.openapis.org/) match your HTTP request/responses used in your tests.

See documentation on [pkg.go.dev](https://pkg.go.dev/openapi.tanna.dev/go/validator).

## Usage

```sh
go get openapi.tanna.dev/go/validator/openapi3
```

```go
import (
	validator "openapi.tanna.dev/go/validator/openapi3"
)

func Test_Handler_Contract(t *testing.T) {
	doc, err := openapi3.NewLoader().LoadFromFile("testdata/fedmodel.yaml")
	if err != nil {
		t.Error("unexpected test setup error reading OpenAPI contract", err)
	}

	rr := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "/apis", nil)
	req.Header.Add("tracing-id", "123")
	v := validator.NewValidator(doc).ForTest(t, rr, req)

	Handler(rr, req)

	// validation happens automagically!
}

```

## Structure

- `openapi3` ([godoc](https://pkg.go.dev/openapi.tanna.dev/go/validator/openapi3)) - OpenAPI 3.x support
