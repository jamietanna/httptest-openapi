package validator

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/getkin/kin-openapi/openapi3filter"
	"github.com/stretchr/testify/assert"
)

type fakeT struct {
	state *state
}

type state struct {
	errored       bool
	messages      []string
	cleanupCalled bool
}

func newFakeT() fakeT {
	return fakeT{
		state: &state{},
	}
}

func Test_FedModel(t *testing.T) {
	type response struct {
		APIVersion string `json:"api-version"`
		Apis       []any  `json:"apis"`
	}
	loader := openapi3.NewLoader()
	doc, err := loader.LoadFromFile("../testdata/fedmodel.yaml")
	assert.Nil(t, err)

	openapi3filter.RegisterBodyDecoder("application/vnd.uk.gov.api.v1alpha+json", jsonBodyDecoder)
	t.Run("When path not found", func(t *testing.T) {
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/not-path", nil)
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectFailure(t, ft)
	})

	t.Run("When path found, but not valid status code", func(t *testing.T) {
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(401)
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/apis", nil)
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectFailure(t, ft)
	})

	t.Run("When path found, and valid status code, but bad content-type", func(t *testing.T) {
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("content-type", "application/json")
			w.WriteHeader(200)
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/apis", nil)
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectFailure(t, ft)
	})

	t.Run("When path found, and valid status code, but bad body", func(t *testing.T) {
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("content-type", "application/vnd.uk.gov.api.v1alpha+json")
			w.WriteHeader(200)
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/apis", nil)
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectFailure(t, ft)
	})

	t.Run("When path found, and valid status code, but bad body contents", func(t *testing.T) {
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("content-type", "application/vnd.uk.gov.api.v1alpha+json")
			w.Write([]byte("{}"))
			w.WriteHeader(200)
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/apis", nil)
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectFailure(t, ft)
	})

	t.Run("When path found, and valid body, but missing headers", func(t *testing.T) {
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("content-type", "application/vnd.uk.gov.api.v1alpha+json")
			resp := response{
				APIVersion: "api.gov.uk/v1alpha",
				Apis:       make([]interface{}, 0),
			}
			bytes, err := json.Marshal(resp)
			assert.Nil(t, err)
			w.Write(bytes)
			w.WriteHeader(200)
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/apis", nil)
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectFailure(t, ft)
	})

	t.Run("When path found, and valid body, but invalid formatted headers", func(t *testing.T) {
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("correlation-id", "foo")
			w.Header().Add("content-type", "application/vnd.uk.gov.api.v1alpha+json")
			resp := response{
				APIVersion: "api.gov.uk/v1alpha",
				Apis:       make([]interface{}, 0),
			}
			bytes, err := json.Marshal(resp)
			assert.Nil(t, err)
			w.Write(bytes)
			w.WriteHeader(200)
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/apis", nil)
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectFailure(t, ft)
	})

	t.Run("When path found, and valid body, and valid mixed case headers", func(t *testing.T) {
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("corRELATion-id", "1e8e7651-3d24-4d41-87be-533030626bc8")
			w.Header().Add("content-TYPe", "application/vnd.uk.gov.api.v1alpha+json")
			resp := response{
				APIVersion: "api.gov.uk/v1alpha",
				Apis:       make([]interface{}, 0),
			}
			bytes, err := json.Marshal(resp)
			assert.Nil(t, err)
			w.Write(bytes)
			w.WriteHeader(200)
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/apis", nil)
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectSuccess(t, ft)
	})

	t.Run("When path found, and valid body, and valid headers", func(t *testing.T) {
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("correlation-id", "1e8e7651-3d24-4d41-87be-533030626bc8")
			w.Header().Add("content-type", "application/vnd.uk.gov.api.v1alpha+json")
			resp := response{
				APIVersion: "api.gov.uk/v1alpha",
				Apis:       make([]interface{}, 0),
			}
			bytes, err := json.Marshal(resp)
			assert.Nil(t, err)
			w.Write(bytes)
			w.WriteHeader(200)
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/apis", nil)
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectSuccess(t, ft)
	})
}

func Test_Petstore(t *testing.T) {
	loader := openapi3.NewLoader()
	doc, err := loader.LoadFromFile("../testdata/petstore-expanded.yaml")
	assert.Nil(t, err)

	type response struct {
		Name string `json:"name"`
		ID   int    `json:"id"`
	}

	t.Run("/pets/id", func(t *testing.T) {
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("content-type", "application/json")

			resp := response{
				Name: "foo",
				ID:   1,
			}
			bytes, err := json.Marshal(resp)
			assert.Nil(t, err)
			w.Write(bytes)
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/pets/123", nil)
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectSuccess(t, ft)
	})

	t.Run("POST /pets", func(t *testing.T) {
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("content-type", "application/json")
			w.Write([]byte(`{"name":"foo", "id":1}`))
		}
		type newPet struct {
			Name string `json:"name"`
		}
		bs, err := json.Marshal(newPet{Name: "name"})
		assert.Nil(t, err)

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("POST", "/pets", bytes.NewReader(bs))
		req.Header.Add("content-type", "application/json")
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectSuccess(t, ft)
	})
}

func Test_Another(t *testing.T) {
	loader := openapi3.NewLoader()
	doc, err := loader.LoadFromFile("../testdata/params.yaml")
	assert.Nil(t, err)

	type response struct {
		Name string `json:"name"`
		ID   int    `json:"id"`
	}

	t.Run("/pets", func(t *testing.T) {
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("content-type", "application/json")

			resp := response{
				Name: "foo",
				ID:   1,
			}
			bytes, err := json.Marshal(resp)
			assert.Nil(t, err)
			w.Write(bytes)
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/pets?tags=foo", nil)
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectSuccess(t, ft)
	})

	t.Run("/header", func(t *testing.T) {
		fmt.Println("_---")
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("content-type", "application/json")

			resp := response{
				Name: "foo",
				ID:   1,
			}
			bytes, err := json.Marshal(resp)
			assert.Nil(t, err)
			w.Write(bytes)
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/header", nil)
		req.Header.Add("session-token", "x.y.z")
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectSuccess(t, ft)
	})

	t.Run("/secure without auth", func(t *testing.T) {
		fmt.Println("_---")
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("{}"))
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/secure", nil)
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectFailure(t, ft)
	})

	t.Run("/secure with auth, but no Authenticator", func(t *testing.T) {
		fmt.Println("_---")
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("{}"))
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/secure", nil)
		req.SetBasicAuth("", "")
		v := NewValidator(doc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectFailure(t, ft)
	})

	t.Run("/secure with auth and Authenticator", func(t *testing.T) {
		ft := newFakeT()
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("content-type", "application/json")
			w.Write([]byte("{}"))
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/secure", nil)
		req.SetBasicAuth("", "")
		v := NewValidatorWithAuthenticator(doc, openapi3filter.NoopAuthenticationFunc).ForTest(ft, rr, req)

		handler(rr, req)

		v.Validate(rr, req)

		expectSuccess(t, ft)
	})
}

func Test_DeferWorksAutomagically(t *testing.T) {
	loader := openapi3.NewLoader()
	doc, err := loader.LoadFromFile("../testdata/healthcheck.yaml")
	assert.Nil(t, err)

	t.Run("/healthcheck", func(t *testing.T) {
		handler := func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(200)
		}

		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/health", nil)
		_ = NewValidator(doc).ForTest(t, rr, req)

		handler(rr, req)
	})
}

func (t fakeT) Helper() {}

func (t fakeT) Error(args ...any) {
	t.state.errored = true
	t.state.messages = append(t.state.messages, fmt.Sprint(args...))
}

func (t fakeT) Errorf(format string, args ...any) {
	t.state.errored = true
	t.state.messages = append(t.state.messages, fmt.Sprintf(format, args...))
}

func (t fakeT) Cleanup(func()) {
	t.state.cleanupCalled = true
}

// stolen from kin-openapi
func jsonBodyDecoder(body io.Reader, header http.Header, schema *openapi3.SchemaRef, encFn openapi3filter.EncodingFn) (interface{}, error) {
	var value interface{}
	if err := json.NewDecoder(body).Decode(&value); err != nil {
		return nil, &openapi3filter.ParseError{Kind: openapi3filter.KindInvalidFormat, Cause: err}
	}
	return value, nil
}

func expectSuccess(t *testing.T, ft fakeT) {
	t.Helper()
	assert.True(t, ft.state.cleanupCalled, "expected the Cleanup() method to have been called")
	assert.False(t, ft.state.errored, "expected ValidResponse to report success, but it reported an error, with messages: ", ft.state.messages)
}

func expectFailure(t *testing.T, ft fakeT) {
	t.Helper()
	assert.True(t, ft.state.cleanupCalled, "expected the Cleanup() method to have been called")
	assert.True(t, ft.state.errored, "expected Validate to report failure, but it was reported successful")
}
