package validator

import (
	"context"
	"net/http"
	"net/http/httptest"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/getkin/kin-openapi/openapi3filter"
	legacyrouter "github.com/getkin/kin-openapi/routers/legacy"
)

// Validator Validator is a container struct for the configuration for a given
// OpenAPI specification
type Validator struct {
	openapi            *openapi3.T
	authenticationFunc openapi3filter.AuthenticationFunc
}

// TestCaseValidator is the test-specific validation, which includes a reference
// to the current testing.T
type TestCaseValidator struct {
	Validator
	t   testingT
	rr  *httptest.ResponseRecorder
	req *http.Request
}

// NewValidator Construct a new Validator
func NewValidator(openapi *openapi3.T) Validator {
	return NewValidatorWithAuthenticator(openapi, openapi3filter.NoopAuthenticationFunc)
}

// NewValidatorWithAuthenticator Construct a new Validator, with custom AuthenticatorFunc for the `openapi3filter`
func NewValidatorWithAuthenticator(openapi *openapi3.T, authenticationFunc openapi3filter.AuthenticationFunc) Validator {
	return Validator{
		openapi:            openapi,
		authenticationFunc: authenticationFunc,
	}
}

// ForTest Construct a new TestCaseValidator for the specific test. Automagically performs Validate call.
func (v Validator) ForTest(t testingT, rr *httptest.ResponseRecorder, req *http.Request) TestCaseValidator {
	tcv := TestCaseValidator{
		Validator: v,
		t:         t,
		rr:        rr,
		req:       req,
	}
	tcv.t.Cleanup(func() {
		tcv.Validate(rr, req)
	})
	return tcv
}

// Validate validates that the given request and recorded response match the
// configured OpenAPI specification. If any validation errors are found, the
// test will be failed
func (v TestCaseValidator) Validate(rr *httptest.ResponseRecorder, req *http.Request) {
	v.t.Helper()

	// avoid issues with Host matching, based on Servers
	v.openapi.Servers = nil

	oaRouter, err := legacyrouter.NewRouter(v.openapi)
	if err != nil {
		v.t.Error(err)
		return
	}

	// ensure that the requested route is found
	route, pathParams, err := oaRouter.FindRoute(req)

	if err != nil {
		v.t.Error("could not find route: ", err)
		return
	}

	// validate the request
	requestValidationInput := &openapi3filter.RequestValidationInput{
		Request:    req,
		PathParams: pathParams,
		Route:      route,
		Options: &openapi3filter.Options{
			AuthenticationFunc: v.authenticationFunc,
		},
	}
	err = openapi3filter.ValidateRequest(context.Background(), requestValidationInput)
	if err != nil {
		v.t.Error("http request is not valid: ", err)
		return
	}

	// validate the response
	responseValidationInput := &openapi3filter.ResponseValidationInput{
		RequestValidationInput: requestValidationInput,
		Status:                 rr.Result().StatusCode,
		Header:                 rr.Result().Header,
		Options: &openapi3filter.Options{
			IncludeResponseStatus: true,
		},
	}
	responseValidationInput.SetBodyBytes(rr.Body.Bytes())
	err = openapi3filter.ValidateResponse(context.Background(), responseValidationInput)
	if err != nil {
		v.t.Error("http response is not valid: ", err)
		return
	}

	// perform additional validation missed from `openapi3filter`
	// https://github.com/getkin/kin-openapi/issues/546
	responseRef := route.Operation.Responses.Status(rr.Result().StatusCode)
	if responseRef == nil {
		responseRef = route.Operation.Responses.Default()
	}

	if responseRef == nil {
		v.t.Error("no response found: ", err)
		return
	}

	for k, s := range responseRef.Value.Headers {
		h := rr.Result().Header.Get(k)
		if h == "" && s.Value.Required {
			v.t.Error("response missing required response header, ", k)
			return
		}
		err := s.Value.Schema.Value.VisitJSON(h)
		if err != nil {
			v.t.Error(err)
			return
		}
	}
}

// testingT is an interface to allow stubbing for test usages, which conforms
// to required methods from testing.T
type testingT interface {
	Helper()
	Error(args ...any)
	Errorf(forat string, args ...any)
	Cleanup(func())
}
